class AddBackgroundImageToCards < ActiveRecord::Migration
  def change
    add_column :project_cards, :background_image, :string
  end
end
