class CreateProjectCards < ActiveRecord::Migration
  def change
    create_table :project_cards do |t|
      t.string :name
      t.text :description
      t.string :location
      t.float :price
      t.string :image1
      t.string :image2
      t.string :image3
      t.text :property

      t.timestamps null: false
    end
  end
end
