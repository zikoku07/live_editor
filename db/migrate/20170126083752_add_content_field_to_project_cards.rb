class AddContentFieldToProjectCards < ActiveRecord::Migration
  def change
    add_column :project_cards, :content, :text
  end
end
