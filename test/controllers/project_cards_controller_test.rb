require 'test_helper'

class ProjectCardsControllerTest < ActionController::TestCase
  setup do
    @project_card = project_cards(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:project_cards)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create project_card" do
    assert_difference('ProjectCard.count') do
      post :create, project_card: {  }
    end

    assert_redirected_to project_card_path(assigns(:project_card))
  end

  test "should show project_card" do
    get :show, id: @project_card
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @project_card
    assert_response :success
  end

  test "should update project_card" do
    patch :update, id: @project_card, project_card: {  }
    assert_redirected_to project_card_path(assigns(:project_card))
  end

  test "should destroy project_card" do
    assert_difference('ProjectCard.count', -1) do
      delete :destroy, id: @project_card
    end

    assert_redirected_to project_cards_path
  end
end
