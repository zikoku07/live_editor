module ApplicationHelper
  def set_style(property, field)
    return unless property.present?
    property_hash = JSON.parse(property)
    field_property = property_hash[field]
    if field_property.present?
      "top: #{field_property['top']}px; left: #{field_property['left']}px; position: absolute !important;"
    end
  end

  def set_image_style(property, field)
    return unless property.present?
    property_hash = JSON.parse(property)
    field_property = property_hash[field]
    if field_property.present?
      "border-radius: #{field_property['borderRadius']}px; opacity: #{field_property['opacity']}"
    end
  end

  def set_background(card)
    if card.background_image_url.present?
      "background: url('#{card.background_image_url}') no-repeat; background-size: cover;"
    else
      "background: #2c2c2c"
    end
  end

  def project_card_template(card)
    "<div id='project-card' class='project-card-frame ui-selectable' data-adaptive-background data-ab-css-background>
    <div class='pin-tier'>
      <div class='pin-grip'>
        <img src='/assets/jcpin-e9b7fe4f4035729da16d92c6318c314f8c17917b657cb6c969908e7ac7cd8a44.png' alt='Jcpin'>
      </div>
    </div>

    <div class='name-wrapper draggable-item editable-container resizable-container selectable-item' style='top: 31px; left: 17px; width: 279px; height: 43px; position: absolute !important;'>
      #{card.name}
    </div>

    <div class='location-wrapper draggable-item resizable-container editable-container selectable-item' style='top: 31px; left: 553px; width: 137px; height: 27px; position: absolute !important;'>
      #{card.location}
    </div>

    <div class='price-wrapper draggable-item resizable-container editable-container selectable-item' style='top: 77px; left: 23px; position: absolute !important;'>
       #{card.price}
    </div>

    <div class='resizable-container selectable-item editable-image' style='top: 96px; left: 467px; width: 223px; height: 155px; position: absolute !important;'>
      <img class='image-preview' id='image1' src='#{card.image1_url}' alt='10497334 907125592647296 875846361286219637 o'>
      <div class='soft-edge'></div>
    </div>

    <div class='resizable-container selectable-item editable-image' style='top: 94px; left: 247px; position: absolute !important; height: 158px;'>
      <img class='image-preview' id='image2' src='#{card.image2_url}' alt='File 1485179115'>
      <div class='soft-edge'></div>
    </div>

    <div class='resizable-container selectable-item editable-image' style='top: 260px; left: 10px; width: 228px; height: 150px; position: absolute !important;'>
      <img class='image-preview' id='image3' data-target-border='border_image3' src='#{card.image3_url}' alt='Products'>
      <div class='soft-edge'></div>
    </div>

    <div class='description-wrapper resizable-container draggable-item editable-container selectable-item ui-selectee ui-draggable ui-draggable-handle ui-resizable' data-container='description' style='top: 268.062px; left: 256px; height: 103px; position: absolute !important;' contenteditable='true'>
      #{card.description}
    </div>

    <div class='pull-right' style='position: absolute; right: 15px; bottom: 15px;'>
      <img style='width: 126px; height: 35px;' src='/assets/logo-95a6241bd2a9a75ad70f5bd39f7090517e9bd828083665f5109e1288649cba9a.png' alt='Logo'>
    </div>
  </div>"
  end

end
