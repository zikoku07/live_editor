class ProjectCard < ActiveRecord::Base

  THEME = ['theme_1.jpeg', 'theme_2.jpeg', 'theme_3.jpeg', 'theme_4.jpeg', 'theme_5.jpeg', 'theme_6.jpeg', 'theme_7.jpeg', 'theme_8.jpeg', 'theme_9.jpeg', 'theme_10.jpeg', 'theme_11.jpeg', 'theme_12.jpeg']

  attr_accessor :helper_image1, :helper_image2, :helper_image3
  mount_uploader :image1, ProjectCardUploader
  mount_uploader :image2, ProjectCardUploader
  mount_uploader :image3, ProjectCardUploader
  mount_uploader :background_image, ProjectCardUploader
  mount_base64_uploader :image1, ProjectCardUploader, file_name: -> { "file_#{DateTime.now.to_i}" }
  mount_base64_uploader :image2, ProjectCardUploader, file_name: -> { "file_#{DateTime.now.to_i}" }
  mount_base64_uploader :image3, ProjectCardUploader, file_name: -> { "file_#{DateTime.now.to_i}" }
  serialize :property, JSON

  # after_create :create_default_template


  def create_default_template
    self.update_attributes(content: ApplicationController.helpers.project_card_template(self))
  end
end
