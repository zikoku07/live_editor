class ProjectCardsController < ApplicationController
  before_action :set_project_card, only: [:show, :edit, :update, :destroy]

  # GET /project_cards
  # GET /project_cards.json
  def index
    @project_cards = ProjectCard.all
  end

  # GET /project_cards/1
  # GET /project_cards/1.json
  def show
  end

  # GET /project_cards/new
  def new
    @project_card = ProjectCard.new
  end

  # GET /project_cards/1/edit
  def edit

  end

  # POST /project_cards
  # POST /project_cards.json
  def create
    @project_card = ProjectCard.new(project_card_params)

    respond_to do |format|
      if @project_card.save
        @project_card.create_default_template
        format.html { redirect_to @project_card, notice: 'Project card was successfully created.' }
        format.json { render :show, status: :created, location: @project_card }
      else
        format.html { render :new }
        format.json { render json: @project_card.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /project_cards/1
  # PATCH/PUT /project_cards/1.json
  def update
    respond_to do |format|
      if @project_card.update(project_card_params)
        format.js {}
        format.html { redirect_to @project_card, notice: 'Project card was successfully updated.' }
        format.json { render :show, status: :ok, location: @project_card }
      else
        format.html { render :edit }
        format.json { render json: @project_card.errors, status: :unprocessable_entity }
      end
    end
  end

  def set_theme
    @card = ProjectCard.find_by_id(params[:project_card_id])
    @card.background_image = File.open(File.join(Rails.root, "/app/assets/images/#{params[:theme]}"))
    @card.save
  end

  def upload_theme
    @card = ProjectCard.find_by_id(params[:project_card_id])
    @card.update_attributes(background_image: params[:background_image])
  end

  def update_image
    card = ProjectCard.find_by_id(params[:project_card_id])
    status = card.update_attribute(params[:field], params[:value])
    p status;
    respond_to do |format|
      format.json { render json: {image: card.send(params[:field]).url} }
    end
  end

  def upload_image
    card = ProjectCard.find_by_id(params[:project_card_id])
    status = card.update_attribute(params[:field_name], params[:image])
    respond_to do |format|
      format.json { render json: {image: card.send(params[:field_name]).url} }
    end
  end

  def reset_theme
    @card = ProjectCard.find_by_id(params[:project_card_id])
    @card.background_image.remove!
    @card.remove_background_image = true
    @card.background_image = ''
    @card.save
    @card.reload
  end

  # DELETE /project_cards/1
  # DELETE /project_cards/1.json
  def destroy
    @project_card.destroy
    respond_to do |format|
      format.html { redirect_to project_cards_url, notice: 'Project card was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_project_card
    @project_card = ProjectCard.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def project_card_params
    params.require(:project_card).permit!
  end
end
